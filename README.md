# Peak Programming

I know I know, this is child's play, we can do better

But come on, ain't this great, amazing even?

![Image of main.c and the program compiled and running](endode_peak_programming.png)

### Why?
Because I'm silly, and this is something silly

### Compile
gcc:
```console
gcc -o peak_programming src/main.c
```
clang:
```console
clang -o peak_programming src/main.c
```
### Run
```console
./peak_programming
```
